<?php

namespace Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine;

use Micro\Component\DependencyInjection\Container;
use Micro\Framework\Kernel\Plugin\AbstractPlugin;
use Micro\Plugin\Amqp\AmqpFacadeInterface;
use Micro\Plugin\AmqpTaskStatus\Adapter\AmqpTaskStatusAdapterInterface;
use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Adapter\DoctrineAdapter;
use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\ORM\DoctrineEntityManagerResolverFactory;
use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Factory\AmqpTaskStatusEntityFactory;
use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Serializer\MessageSerializerInterface;
use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Serializer\Serializer;
use Micro\Plugin\AmqpTaskStatus\Plugin\AmqpTaskStatusAdapterProviderInterface;
use Micro\Plugin\Doctrine\DoctrineFacadeInterface;
use Micro\Plugin\Logger\LoggerFacadeInterface;

class AmqpTaskStatusDoctrineAdapterPlugin extends AbstractPlugin implements AmqpTaskStatusAdapterProviderInterface
{
    /**
     * @var Container
     */
    private Container $container;

    private ?MessageSerializerInterface $messageSerializer = null;

    /**
     * @param Container $container
     * @return void
     */
    public function provideDependencies(Container $container): void
    {
        $this->container = $container;
    }

    /**
     * @return AmqpTaskStatusAdapterInterface
     */
    public function provideAmqpTaskStatusAdapter(): AmqpTaskStatusAdapterInterface
    {
        return $this->createAdapter();
    }

    /**
     * @return AmqpTaskStatusAdapterInterface
     */
    protected function createAdapter(): AmqpTaskStatusAdapterInterface
    {
        return new DoctrineAdapter(
            $this->createDoctrineEntityManagerResolverFactory(),
            $this->createAmqpTaskStatusFactory(),
            $this->lookupLoggerFacade()
        );
    }

    /**
     * @return DoctrineEntityManagerResolverFactory
     */
    protected function createDoctrineEntityManagerResolverFactory(): DoctrineEntityManagerResolverFactory
    {
        return new DoctrineEntityManagerResolverFactory(
            $this->lookupDoctrineFacade(),
            $this->configuration
        );
    }

    /**
     * @return AmqpFacadeInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function lookupAmqpFacadeInterface(): AmqpFacadeInterface
    {
        return $this->container->get(AmqpFacadeInterface::class);
    }

    /**
     * @return DoctrineFacadeInterface
     */
    protected function lookupDoctrineFacade(): DoctrineFacadeInterface
    {
        return $this->container->get(DoctrineFacadeInterface::class);
    }

    /**
     * @return LoggerFacadeInterface
     */
    protected function lookupLoggerFacade(): LoggerFacadeInterface
    {
        return $this->container->get(LoggerFacadeInterface::class);
    }

    private function createSerializer(): MessageSerializerInterface
    {
        if($this->messageSerializer === null) {
            $this->messageSerializer = new Serializer(
                $this->lookupAmqpFacadeInterface()
            );
        }

        return $this->messageSerializer;
    }

    /**
     * @return AmqpTaskStatusEntityFactory
     */
    private function createAmqpTaskStatusFactory(): AmqpTaskStatusEntityFactory
    {
        return new AmqpTaskStatusEntityFactory(
            $this->createSerializer()
        );
    }
}
