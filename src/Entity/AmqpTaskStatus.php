<?php

namespace Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Entity;

use Doctrine\ORM\Mapping as ORM;
use Micro\Plugin\Amqp\Business\Message\MessageInterface;
use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Serializer\MessageSerializerInterface;
use Micro\Plugin\AmqpTaskStatus\Model\TaskStatusInterface;

// phpcs:ignoreFile

/**
 * @ORM\Entity
 * @ORM\Table(name="micro_amqp_task_status")
 * @ORM\HasLifecycleCallbacks
 */
#[ORM\Entity]
#[ORM\Table(name: 'micro_amqp_task_status')]
#[ORM\HasLifecycleCallbacks]
class AmqpTaskStatus implements TaskStatusInterface
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    #[ORM\Column(name: 'created_at', type: 'datetime', nullable: false)]
    private \DateTimeInterface $createdAt;

    /**
     * @var \DateTimeInterface|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    #[ORM\Column(name: 'updated_at', type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="task_id", type="integer", unique=true, nullable=false)
     */
    #[ORM\Column(name: 'task_id', type: 'string', unique: true, nullable: false)]
    private string $taskId;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", unique=false, nullable=false)
     */
    #[ORM\Column(name: 'status', type: 'integer', unique: false, nullable: false)]
    private int $status;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    #[ORM\Column(name: 'serializedMessage', type: 'text', nullable: false)]
    private string $serializedMessage;

    /**
     * @var string|null
     *
     * @ORM\Column(name="serializedException", type="text", nullable=true)
     */
    #[ORM\Column(name: 'serializedException', type: 'text', nullable: true)]
    private ?string $serializedException = null;

    /**
     * @var MessageSerializerInterface
     */
    private MessageSerializerInterface $messageSerializer;

    /**
     * @param MessageSerializerInterface $messageSerializer
     * @param MessageInterface $message
     * @param string $taskId
     * @param int $status
     */
    public function __construct(
        MessageSerializerInterface $messageSerializer,
    private MessageInterface $message,
        string $taskId,
        int $status
    )
    {
        $this->setSerializer($messageSerializer);
        $this->taskId    = $taskId;
        $this->status    = $status;
        $this->createdAt = new \DateTime('now');

        $this->serializeMessage();
    }

    protected function serializeMessage(): void
    {
        $this->serializedMessage = $this->messageSerializer->serialize($this->message);
    }

    /**
     * @param MessageSerializerInterface $messageSerializer
     *
     * @return $this
     */
    public function setSerializer(MessageSerializerInterface $messageSerializer)
    {
        $this->messageSerializer = $messageSerializer;

        return $this;
    }

    /**
     * @return void
     *
     * @ORM\PreUpdate
     */
    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        $this->updatedAt = new \DateTime('now');
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * {@inheritDoc}
     */
    public function getTaskId(): string
    {
        return $this->taskId;
    }

    /**
     * {@inheritDoc}
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getMessage(): MessageInterface
    {
        if(!$this->message) {
            $this->message = $this->messageSerializer->deserialize($this->serializedMessage);
        }

        return $this->message;
    }

    /**
     * @return string
     */
    public function getSerializedException(): ?string
    {
        return $this->serializedException;
    }

    /**
     * {@inheritDoc}
     */
    public function getException(): ?\Throwable
    {
        return unserialize($this->serializedException, \Throwable::class);
    }

    /**
     * {@inheritDoc}
     */
    public function setException(?\Throwable $throwable): self
    {
        $this->serializedException = serialize($throwable);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
}
