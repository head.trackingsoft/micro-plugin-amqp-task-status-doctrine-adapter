<?php

namespace Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Client;

use Micro\Plugin\AmqpTaskStatus\Business\Client\StatusClientFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Client\StatusClientInterface;

class ClientFactory implements StatusClientFactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function create(): StatusClientInterface
    {
        return new Client();
    }
}
