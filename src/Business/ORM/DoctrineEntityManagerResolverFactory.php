<?php

namespace Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\ORM;

use Micro\Plugin\Doctrine\DoctrineFacadeInterface;

class DoctrineEntityManagerResolverFactory implements DoctrineEntityManagerResolverFactoryInterface
{
    /**
     * @param DoctrineFacadeInterface $doctrineFacade
     */
    public function __construct(
    private DoctrineFacadeInterface $doctrineFacade
    )
    {
    }

    /**
     * @return DoctrineEntityManagerResolverInterface
     */
    public function create(): DoctrineEntityManagerResolverInterface
    {
        return new DoctrineEntityManagerResolver($this->doctrineFacade);
    }
}
