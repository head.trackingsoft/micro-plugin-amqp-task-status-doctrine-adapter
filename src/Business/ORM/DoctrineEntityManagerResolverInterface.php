<?php

namespace Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\ORM;

use Doctrine\ORM\EntityManagerInterface;

interface DoctrineEntityManagerResolverInterface
{
    /**
     * @return EntityManagerInterface
     */
    public function resolveClient(): EntityManagerInterface;

    /**
     * @return EntityManagerInterface
     */
    public function resolveManager(): EntityManagerInterface;
}
