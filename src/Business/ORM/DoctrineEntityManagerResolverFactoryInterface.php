<?php

namespace Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\ORM;

interface DoctrineEntityManagerResolverFactoryInterface
{
    /**
     * @return DoctrineEntityManagerResolverInterface
     */
    public function create(): DoctrineEntityManagerResolverInterface;
}
