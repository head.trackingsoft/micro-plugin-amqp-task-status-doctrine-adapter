<?php

namespace Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\ORM;

use Doctrine\ORM\EntityManagerInterface;
use Micro\Plugin\Doctrine\DoctrineFacadeInterface;

class DoctrineEntityManagerResolver implements DoctrineEntityManagerResolverInterface
{
    /**
     * @param DoctrineFacadeInterface $doctrineFacade
     */
    public function __construct(private DoctrineFacadeInterface $doctrineFacade)
    {
    }

    /**
     * @return EntityManagerInterface
     */
    public function resolveClient(): EntityManagerInterface
    {
        return $this->doctrineFacade->getManager();
    }

    /**
     * @return EntityManagerInterface
     */
    public function resolveManager(): EntityManagerInterface
    {
        return $this->doctrineFacade->getManager();
    }
}
