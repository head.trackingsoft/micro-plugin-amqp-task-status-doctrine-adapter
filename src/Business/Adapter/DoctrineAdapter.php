<?php

namespace Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Adapter;

use Micro\Plugin\AmqpTaskStatus\Adapter\AmqpTaskStatusAdapterInterface;
use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Client\ClientFactory;
use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Manager\ManagerFactory;
use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\ORM\DoctrineEntityManagerResolverFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Factory\AmqpTaskStatusEntityFactory;
use Micro\Plugin\AmqpTaskStatus\Business\Client\StatusClientFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Manager\StatusManagerFactoryInterface;
use Micro\Plugin\Logger\LoggerFacadeInterface;

class DoctrineAdapter implements AmqpTaskStatusAdapterInterface
{
    /**
     * @param DoctrineEntityManagerResolverFactoryInterface $doctrineEntityManagerResolverFactory
     * @param AmqpTaskStatusEntityFactory $amqpTaskStatusEntityFactory
     * @param LoggerFacadeInterface $loggerFacade
     */
    public function __construct(
    private DoctrineEntityManagerResolverFactoryInterface $doctrineEntityManagerResolverFactory,
    private AmqpTaskStatusEntityFactory $amqpTaskStatusEntityFactory,
    private LoggerFacadeInterface $loggerFacade
    )
    {
    }

    /**
     * @return StatusManagerFactoryInterface
     */
    public function getStatusManagerFactory(): StatusManagerFactoryInterface
    {
        return new ManagerFactory(
            $this->doctrineEntityManagerResolverFactory,
            $this->amqpTaskStatusEntityFactory,
            $this->loggerFacade->getLogger()
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getClientManagerFactory(): StatusClientFactoryInterface
    {
        return new ClientFactory();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'doctrine';
    }
}
