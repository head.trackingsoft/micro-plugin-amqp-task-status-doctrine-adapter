<?php

namespace Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Serializer;

use Micro\Plugin\Amqp\AmqpFacadeInterface;
use Micro\Plugin\Amqp\Business\Message\MessageInterface;

class Serializer implements MessageSerializerInterface
{
    /**
     * @param AmqpFacadeInterface $amqpFacade
     */
    public function __construct(private AmqpFacadeInterface $amqpFacade)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function serialize(MessageInterface $message): string
    {
        return $this->amqpFacade->serializeMessage($message);
    }

    /**
     * {@inheritDoc}
     */
    public function deserialize(string $messageContent): MessageInterface
    {
        return $this->amqpFacade->deserializeMessage($messageContent);
    }
}
