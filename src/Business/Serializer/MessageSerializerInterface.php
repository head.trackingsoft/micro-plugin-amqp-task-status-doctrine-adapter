<?php

namespace Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Serializer;

use Micro\Plugin\Amqp\Business\Message\MessageInterface;

interface MessageSerializerInterface
{
    /**
     * @param MessageInterface $message
     *
     * @return string
     */
    public function serialize(MessageInterface $message): string;

    /**
     * @param string $messageContent
     *
     * @return MessageInterface
     */
    public function deserialize(string $messageContent): MessageInterface;
}
