<?php

namespace Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Micro\Plugin\Amqp\Business\Message\MessageInterface;
use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Entity\AmqpTaskStatus;
use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Factory\AmqpTaskStatusEntityFactory;
use Micro\Plugin\AmqpTaskStatus\Business\Manager\StatusManagerInterface;
use Psr\Log\LoggerInterface;

class Manager implements StatusManagerInterface
{

    /**
     * @param EntityManagerInterface $entityManager
     * @param AmqpTaskStatusEntityFactory $amqpTaskStatusEntityFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
    private EntityManagerInterface $entityManager,
    private AmqpTaskStatusEntityFactory $amqpTaskStatusEntityFactory,
    private LoggerInterface $logger
    )
    {
    }

    public function changeStatus(string $taskId, int $status): void
    {
        /** @var AmqpTaskStatus $taskStatusEntity */
        $taskStatusEntity = $this->entityManager
            ->getRepository($this->amqpTaskStatusEntityFactory->getEntityClass())
            ->findOneBy([
                'taskId' => $taskId
            ]);

        if($taskStatusEntity === null) {
            $this->logger->warning(sprintf('Can not change status on database. Task "%s" is not registered', $taskId), [
                'class' => __CLASS__,
                'task_id' => $taskId,
                'status'    => $status
            ]);

            return;
        }

        $taskStatusEntity->setStatus($status);
        $this->save($taskStatusEntity);
    }

    /**
     * {@inheritDoc}
     */
    public function register(string $taskId, MessageInterface $message): void
    {
        $taskStatusEntity = $this->amqpTaskStatusEntityFactory->create($taskId, $message);

        $this->save($taskStatusEntity);
    }

    /**
     * @param AmqpTaskStatus $taskStatusEntity
     * @return void
     */
    protected function save(AmqpTaskStatus $taskStatusEntity): void
    {
        $this->entityManager->persist($taskStatusEntity);
        $this->entityManager->flush();
    }
}
