<?php

namespace Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Manager;

use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\ORM\DoctrineEntityManagerResolverFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Factory\AmqpTaskStatusEntityFactory;
use Micro\Plugin\AmqpTaskStatus\Business\Manager\StatusManagerFactoryInterface;
use Micro\Plugin\AmqpTaskStatus\Business\Manager\StatusManagerInterface;
use Psr\Log\LoggerInterface;

class ManagerFactory implements StatusManagerFactoryInterface
{
    /**
     * @param DoctrineEntityManagerResolverFactoryInterface $doctrineEntityManagerResolverFactory
     * @param AmqpTaskStatusEntityFactory $amqpTaskStatusEntityFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
    private DoctrineEntityManagerResolverFactoryInterface $doctrineEntityManagerResolverFactory,
    private AmqpTaskStatusEntityFactory $amqpTaskStatusEntityFactory,
    private LoggerInterface $logger
    )
    {
    }

    /**
     * @return StatusManagerInterface
     */
    public function create(): StatusManagerInterface
    {
        return new Manager(
            $this->doctrineEntityManagerResolverFactory->create()->resolveManager(),
            $this->amqpTaskStatusEntityFactory,
            $this->logger
        );
    }
}
