<?php

namespace Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Factory;

use Micro\Plugin\Amqp\Business\Message\MessageInterface;
use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Business\Serializer\MessageSerializerInterface;
use Micro\Plugin\AmqpTaskStatus\Adapter\Doctrine\Entity\AmqpTaskStatus;
use Micro\Plugin\AmqpTaskStatus\Model\TaskStatus;

class AmqpTaskStatusEntityFactory
{
    /**
     * @param MessageSerializerInterface $messageSerializer
     */
    public function __construct(private MessageSerializerInterface $messageSerializer)
    {

    }

    /**
     * @param string $taskId
     * @param MessageInterface $message
     * @return AmqpTaskStatus
     */
    public function create(string $taskId, MessageInterface $message): AmqpTaskStatus
    {
        return new AmqpTaskStatus(
            $this->messageSerializer,
            $message,
            $taskId,
            TaskStatus::PENDING,
        );
    }

    /**
     * @return string
     */
    public function getEntityClass(): string
    {
        return AmqpTaskStatus::class;
    }
}
